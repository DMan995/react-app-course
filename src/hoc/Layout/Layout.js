import React, {Component} from 'react';
import Aux from '../Aux/Aux';
import classes from './Layout.css';
import Toolbar from '../../components/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer'

class Layout extends Component{
   state={
       sideDrawer:false
   }
     
    closeSideDrawer=()=>{
       this.setState({sideDrawer:false})
   }
   toggleSideDrawer=()=>{
    this.setState((prevState)=>{
        return {sideDrawer:!prevState.sideDrawer};
    });
}

    render(){
        return (
            <Aux>
            <div>
                <Toolbar openSideDrawer={this.toggleSideDrawer}/>
                <SideDrawer 
                show={this.state.sideDrawer}
                close={this.closeSideDrawer}
                />
            </div>
            <main className={classes.Content}>
            {this.props.children}
        </main>
        </Aux>
        );

    }
}

export default Layout;