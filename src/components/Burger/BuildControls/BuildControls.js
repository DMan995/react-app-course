import React from 'react'
import BuildControl from './BuildControl/BuildControl';
import classes from './BuildControls.css'

const buildControls =(props)=>{
    const controls =[
        {label: 'Salad', type:'salad'},
        {label: 'Bacon', type:'bacon'},
        {label: 'Cheese', type:'cheese'},
        {label: 'Meat', type:'meat'}
    ]
   return(
       <div className={classes.BuildControls}>
           <p>Total price: <strong>{props.price} </strong></p>
        { controls.map(cntrl =>{
            return <BuildControl label={cntrl.label}
                    key={cntrl.label}
                    addFunc={()=>props.add(cntrl.type)}
                    removeFunc={()=>props.remove(cntrl.type)}
                    disabled={props.disable[cntrl.type]}
            />
        })}
        <button disabled={props.purchasable} className={classes.OrderButton} onClick={props.purchasing}>PURCHASE NOW</button>
      </div>
   )
}

export default buildControls;