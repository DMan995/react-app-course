import React from 'react';
import Aux from '../../../hoc/Aux/Aux';
import Button from '../../Button//Button';
const orderSummary=(props)=>{
    const summaryItems=Object.keys(props.ingredients).map(igk =>{
    return <li key={igk}>{igk}:{props.ingredients[igk]}</li>
    });
    return( 
        <Aux>
          <strong>Total amount: {props.total.toFixed(2)}$</strong>
         <p>Perfectly chosen burger with delicius ingredients:</p>
       <ul >
          {summaryItems}
       </ul>
       <p><strong>Continue to checkout?</strong></p>
       <Button btnType='Danger' clicked={props.cancelClick}>Cancel Order</Button>
       <Button btnType='Success' clicked={props.continueClick}>Continue to checkout</Button>
        </Aux>
    );

}

export default orderSummary;