import React from 'react';
import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo';
import classes from './SideDrawer.css'
import Backdrop from '../../UI/Backdrop/Backdrop';
import Aux from '../../../hoc/Aux/Aux';
const sideDrawer=(props)=>{
    let sideDrawerClasses=[classes.SideDrawer,classes.Close];
    if(props.show){
         sideDrawerClasses=[classes.SideDrawer,classes.Open];
    }
   
    return(
       
        <Aux>
            <Backdrop show={props.show} backdropClicked={props.close}/>
            <div className={sideDrawerClasses.join(' ')}>
           <div className={classes.Logo}>
           <Logo/>
           </div>
           <nav>
               <NavigationItems/>
           </nav>
      </div>
        </Aux>
    

    )
}
export default sideDrawer;
