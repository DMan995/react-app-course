import React from 'react';
import classes from './Toolbar.css'
import Logo from '../Logo/Logo';
import NavigationItems from '../Navigation/NavigationItems/NavigationItems'
const toolbar =(props)=>{
return(
<header className={classes.Toolbar}>
<div onClick={props.openSideDrawer} className={classes.DrawerToggle}>
            <div></div>
            <div></div>
            <div></div>
            </div>
           <Logo/>
           <nav className={classes.HideDesktop}>
           <NavigationItems />
           </nav>
     
        
 
    </header>
)
    

}
export default toolbar;