import React,{ Component } from "react";
import Aux from '../../hoc/Aux/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI//Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
const INGREDIENT_PRICES={
  salad:0.5,
  cheese:0.4,
  meat:1.3,
  bacon:0.7
}

class BurgerBuilder extends Component{
  state ={
    ingredients:{
      salad:0,
      cheese:0,
      bacon:0,
      meat:0,
    },
     totalPrice:4,
     purchasable:true,
     purschasing:false
  }

  updatePurchasable(ingredientsData){
   const itemsCount=Object.keys(ingredientsData).map(type=>{
     return ingredientsData[type];
   }).reduce((sum,el)=>{
         return sum+el;
   },0);
    this.setState({purchasable: itemsCount<=0})

  }

  updatepurschasing=()=>{
    this.setState({purschasing:true});
  }
  cancelpurschasing=()=>{
    this.setState({purschasing:false});
  }
  succesPurchasing=()=>{
    const resetIngredients={...this.state.ingredients}
    for(let key in resetIngredients){
       resetIngredients[key]=0;
    }
    this.setState({purschasing:false,ingredients:resetIngredients,totalPrice:4});
   setTimeout(()=>{
    alert("Thank you for ordering from us. Enjoy your delicious sandwich!")
   },500)
  }


  addIngredinetHandler=(type)=>{
   const oldState=this.state.ingredients[type];
   const newState=oldState+1;
   const updatedIngredients={
      ...this.state.ingredients
   }
   updatedIngredients[type]=newState;
   const newPrice=this.state.totalPrice+INGREDIENT_PRICES[type];
   this.setState({totalPrice:newPrice,ingredients:updatedIngredients})
   this.updatePurchasable(updatedIngredients);
  } 
  removeIngredinetHandler=(type)=>{
    const oldState=this.state.ingredients[type];
    const newState=oldState-1; 
    if(oldState!==0){
      const updatedIngredients={
        ...this.state.ingredients
     }
     updatedIngredients[type]=newState;
     const newPrice=(this.state.totalPrice-INGREDIENT_PRICES[type]);
     this.setState({totalPrice:newPrice,ingredients:updatedIngredients})
     this.updatePurchasable(updatedIngredients);
    }
   } 
 
  render(){
    const disabledInfo={...this.state.ingredients};
    for(let key in disabledInfo){
      disabledInfo[key]=disabledInfo[key] >0 ? false:true;
    }
      return (
         <Aux>
           <Modal show={this.state.purschasing} backdropClicked={this.cancelpurschasing}>
           <OrderSummary  ingredients={this.state.ingredients} 
           total={this.state.totalPrice}
           cancelClick={this.cancelpurschasing}
           continueClick={this.succesPurchasing}
           />
           </Modal>
           <Burger ingredients={this.state.ingredients} />
           <BuildControls
           remove={this.removeIngredinetHandler} 
           add={this.addIngredinetHandler}
           disable={disabledInfo}
           price={this.state.totalPrice.toFixed(2)}
           purchasable={this.state.totalPrice<=4}
             //purchasable={this.state.purchasable}
             purchasing={this.updatepurschasing}
           ></BuildControls>
         </Aux>
      );
  }
}

export default BurgerBuilder;